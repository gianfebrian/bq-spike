package com.gianfebrian;

import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;

public class Filter {
    public static void main(String[] args) {
        String project = "spikey-infra";
        String region = "us-central1";
        String inputGcs = "gs://gflow-spike/us-covid-county-population-*";
        String outputGcs = "gs://gflow-spike/filtered";

        PipelineOptions options = PipelineOptionsFactory.create();
        DataflowPipelineOptions dataflowOptions = options.as(DataflowPipelineOptions.class);
        dataflowOptions.setRunner(DataflowRunner.class);
        dataflowOptions.setProject(project);
        dataflowOptions.setRegion(region);

        Pipeline p = Pipeline.create(options);

        p.apply(TextIO.read().from(inputGcs))
            .apply(
                ParDo.of(
                    new DoFn<String, String>() {
                        @ProcessElement
                        public void processElement(ProcessContext c) {
                            String line = c.element();
                            String[] columns = line.split(",");
                            String criteria = columns[3];
                            if (criteria != "null") {
                                c.output(line);
                            }
                        }
                    }
                )
            )
            .apply(TextIO.write().to(outputGcs).withSuffix(".csv"));

        p.run();
    }
}
