package com.gianfebrian;

import com.google.api.services.bigquery.model.TableRow;
import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Extract {
    public static void main(String[] args) {
        String project = "spikey-infra";
        String dataset = "test";
        String region = "us-central1";
        String table = "covid-open-data";
        String outputGcs = "gs://gflow-spike/us-covid-county-population";

        PipelineOptions options = PipelineOptionsFactory.create();
        DataflowPipelineOptions dataflowOptions = options.as(DataflowPipelineOptions.class);
        dataflowOptions.setRunner(DataflowRunner.class);
        dataflowOptions.setProject(project);
        dataflowOptions.setRegion(region);

        Pipeline p = Pipeline.create(options);

        p.apply(
            "read from bq using query",
            BigQueryIO.readTableRows()
                .fromQuery(
                    String.format("SELECT date, datacommons_id, country_name, new_recovered" +
                        " FROM `%s.%s.%s` LIMIT 10000000", project, dataset, table)
                )
            .usingStandardSql()
        ).apply(
            "map elements",
            ParDo.of(
                new DoFn<TableRow, String>() {
                    @ProcessElement
                    public void processElement(ProcessContext c) {
                        TableRow row = c.element();
                        if (row == null) {
                            c.output("");
                            return;
                        }

                        ArrayList<String> values = new ArrayList<>();
                        values.add((String) row.get("date"));
                        values.add((String) row.get("datacommons_id"));
                        values.add((String) row.get("country_name"));
                        values.add((String) row.get("new_recovered"));

                        String stringRow = values
                            .stream()
                            .map(String::valueOf)
                            .collect(Collectors.joining(","));

                        c.output(stringRow);
                    }
                }
            )
        ).apply(TextIO.write().to(outputGcs).withSuffix(".csv"));

        p.run();
    }
}
